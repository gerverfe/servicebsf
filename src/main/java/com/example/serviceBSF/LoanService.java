package com.example.serviceBSF;

import _3gpp.gba.gbaservice._2010_02.RequestBootstrappingInfoFault;
import _3gpp.gba.gbaservice._2010_02.RequestBootstrappingInfoRequest;
import _3gpp.gba.gbaservice._2010_02.RequestBootstrappingInfoResponse;
import org.springframework.stereotype.Service;
import com.example.serviceBSF.repository.MatchImpi;
import com.example.serviceBSF.repository.ResponseRepository;


@Service
public class LoanService {

    public RequestBootstrappingInfoFault faulResponse(int faulCode){
        RequestBootstrappingInfoFault faulreq = new RequestBootstrappingInfoFault();
        if(faulCode == 5403){
            faulreq.setErrorCode(5403);
            faulreq.setErrorText(" B-TID tuple nonexistent ");
        }else if(faulCode == 5402){
            faulreq.setErrorCode(5402);
            faulreq.setErrorText(" NAF-Id not alowed for UE ");
        }else{
            faulreq.setErrorCode(5000);
            faulreq.setErrorText(" Request not found ");
        }
        return faulreq;
    }

    public RequestBootstrappingInfoResponse checkBsf(RequestBootstrappingInfoRequest request) {
        RequestBootstrappingInfoResponse respuesta = new RequestBootstrappingInfoResponse();
        ResponseRepository repositorio = new ResponseRepository();
        repositorio.initData();

        MatchImpi buscoImpi = repositorio.findImpi(request.getBtid());
        System.out.println(request.getBtid());
   ;

        if (request.getNafid() == null) {
            faulResponse(5402);
            respuesta = null;
            System.out.println("ingreso en el primer if");
        } else {
            if (buscoImpi.getImpi() == null) {
                faulResponse(5403);
                respuesta = null;
                System.out.println("ingreso en el segundo if");
            } else {
                respuesta = repositorio.findResBSF(buscoImpi.getImpi());
                System.out.println("ingreso en el tercer if");
            }
        }

        return respuesta;

    }

    }

package com.example.serviceBSF;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceBsfApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceBsfApplication.class, args);
	}

}

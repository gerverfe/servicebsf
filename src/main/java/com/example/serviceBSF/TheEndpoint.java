package com.example.serviceBSF;

import _3gpp.gba.gbaservice._2010_02.RequestBootstrappingInfoRequest;
import _3gpp.gba.gbaservice._2010_02.RequestBootstrappingInfoResponse;
import com.example.serviceBSF.repository.ResponseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.example.serviceBSF.LoanService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Endpoint
public class TheEndpoint {
    private static final String NAMESPACE = "urn:3gpp:gba:GBAService:2010-02";

    private ResponseRepository responseRepository;

    @Autowired
    public TheEndpoint(ResponseRepository responseRepository){
        this.responseRepository = responseRepository;
    }


    @PayloadRoot(namespace = NAMESPACE, localPart = "requestBootstrappingInfoRequest")
    @ResponsePayload
    public RequestBootstrappingInfoResponse getResponse(@RequestPayload RequestBootstrappingInfoRequest request){
        //la hora
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        //resourse
        RequestBootstrappingInfoResponse response=new RequestBootstrappingInfoResponse();
        response.setImpi(responseRepository.findResBSF(request.getBtid()).getImpi());
        response.setMeKeyMaterial(responseRepository.findResBSF(request.getBtid()).getMeKeyMaterial());
        response.setUiccKeyMaterial(responseRepository.findResBSF(request.getBtid()).getUiccKeyMaterial());
        response.setKeyExpiryTime(dateFormat.format(date));
        response.setBootstrappingInfoCreationTime(dateFormat.format(date));
        response.setGbaType(responseRepository.findResBSF(request.getBtid()).getGbaType());
        response.setUssList(responseRepository.findResBSF(request.getBtid()).getUssList());
        return response;
    }






//    @Autowired
//    private LoanService service;
//
//    @PayloadRoot(namespace = NAMESPACE, localPart = "requestBootstrappingInfoRequest")
//    @ResponsePayload
//    public RequestBootstrappingInfoResponse getLoanStatus(@RequestPayload RequestBootstrappingInfoRequest request) {
//        return service.checkBsf(request); //lo que se le responde al usuario
//    }

}

package com.example.serviceBSF.repository;

import _3gpp.gba.gbaservice._2010_02.RequestBootstrappingInfoResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

@Component
public class ResponseRepository {
    public static final Map<String, RequestBootstrappingInfoResponse> resBSF = new HashMap<>();
    public static final Map<String, MatchImpi> matchBSF = new HashMap<>();

    @PostConstruct
    public void initData(){
        RequestBootstrappingInfoResponse res1 = new RequestBootstrappingInfoResponse();
        res1.setImpi("12345678");
        res1.setMeKeyMaterial("Ks_NAF/Ks_ext_NAF");
        res1.setUiccKeyMaterial("Ks_int_NAF");
        //res1.setKeyExpiryTime(yourInstant);
        //res1.setBootstrappingInfoCreationTime(yourInstant); las fechas estan comentadas porque las inserto en el endpoint
        res1.setGbaType("2");
        res1.setUssList("xml GUSS");

        resBSF.put(res1.getImpi(), res1);

        MatchImpi match1 = new MatchImpi();
        match1.setImpi("12345678");
        match1.setBtid("qwerty");//buscar uno que llegue

        matchBSF.put(match1.getImpi(), match1);

        RequestBootstrappingInfoResponse res2 = new RequestBootstrappingInfoResponse();
        res2.setImpi("87654321");
        res2.setMeKeyMaterial("Ks_NAF/Ks_ext_NAF");
        res2.setUiccKeyMaterial("Ks_int_NAF");
        //res2.setKeyExpiryTime(yourInstant);
        //res2.setBootstrappingInfoCreationTime(yourInstant);//le pongo la fehca de la llamada - 20min
        res2.setGbaType("2");
        res2.setUssList("xml GUSS");

        resBSF.put(res2.getImpi(), res2);

        MatchImpi match2 = new MatchImpi();
        match2.setImpi("87654321");
        match2.setBtid("ytrewq");//buscar uno que llegue

        matchBSF.put(match2.getImpi(), match2);

        RequestBootstrappingInfoResponse res3 = new RequestBootstrappingInfoResponse();
        res3.setImpi("987654");
        res3.setMeKeyMaterial("Ks_NAF/Ks_ext_NAF");
        res3.setUiccKeyMaterial("Ks_int_NAF");
        //res3.setKeyExpiryTime(yourInstant.toString());
        //res3.setBootstrappingInfoCreationTime(yourInstant);//le pongo la fehca de la llamada - 20min
        res3.setGbaType("2");
        res3.setUssList("xml GUSS");

        resBSF.put(res3.getImpi(), res3);

        MatchImpi match3 = new MatchImpi();
        match3.setImpi("987654");
        match3.setBtid("asdfgh");//buscar uno que llegue

        matchBSF.put(match3.getImpi(), match3);
    }

    //busco al response
    public RequestBootstrappingInfoResponse findResBSF(String impi) {
        Assert.notNull(impi, "null impi, find resBSF");
        return resBSF.get(impi);
    }

    //busco el impi
    public MatchImpi findImpi(String btid) {
        Assert.notNull(btid, "null impi, find matchBSF");
        return matchBSF.get(btid);
    }
}

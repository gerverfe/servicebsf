package com.example.serviceBSF.repository;

public class MatchImpi {
    protected String impi;
    protected String btid;

    public String getImpi() {
        return impi;
    }

    public void setImpi(String impi) {
        this.impi = impi;
    }

    public String getBtid() {
        return btid;
    }

    public void setBtid(String btid) {
        this.btid = btid;
    }
}
